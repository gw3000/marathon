# Packliste für den Wettkamp

Es folgt eine kurze Zusammenstellung aller nötigen und unnötigen Dinge die man
für den Wettkampf (egal ob Marathon oder 5km Rennen) benötigt.

## Bekleidung

- langes Oberteil
- kurzes Oberteil (mit oder ohne Ärmel hängt von den Witterungsbedingungen ab)
- lange Lauftight
- kurze Lauftight / oder Laufhose
- ein Paar Laufsocken (am besten schon mal getragen, ohne sie danach zu waschen)
- ein Paar Kompressionsstulpen
- Kapuzenpullover oder Jacke
- Klappsband oder Mütze
- Pulswärmer als Schweisabsorber
- zwei Paar Laufschuhe der aktuellen Saison
- evtl ein neues Paar Schnürsenkel


## Technik

- Pulsuhr
- Pulsgurt
- GPS-Sensor
- __Startnummernchip__ sofern bei der Anmeldung mit angegeben
- evtl. Schrittzähler


## Medikamente

- Kopfschmerzmittel
- Vaseline
- Wärmecreme
- Schmerzsalbe
- Nippelpflaster

## Verpflegung

- Wasserflasche (Trinkflasche)

