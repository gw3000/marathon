# Marathon - Tapering, Vor- und Nachbereitung, Lauftaktik und mehr

Ein kleiner Leitfaden für die Tage vor und nach dem Marathon und den Wettkampftag.


## Der Tag vor dem Marathon

* Tag davor (zumeist Samstag) nicht übermäßig viel laufen oder stehen, kein Sightseeing
* Pasta essen muss nicht (ist Marketing!), wichtig ist nicht zu viel essen, fleischlos, eher Gemüse essen, genug Trinken
* kein Alkohol, kein Fisch, kein Fleisch essen
* nur vegetarische Kost
* genügend Trinken um gut hydriert in die Nacht zu gehen (nicht zu viel Trinken)
* Ruhe ist wichtig (vielleicht ein Mittagsschlaf halten)
* bei schlechten Marathonschlaf keinen Stress machen!!!! Wichtig ist sind die Tage/Wochen zuvor >> viel Schlafen


## Komplex Trinken

* Test: Prüfen, wieviel über die Nacht an Flüssigkeit verloren wird (mit der Waage)
* z.B. 0,8l an Wasser in der Nacht verloren: 0,8l am nächsten Morgen auffüllen! vor dem Marathon nicht mehr aufnehmen
* zwei Stunden vor dem Lauf sollte Wasseraufnahme abgeschlossen sein
* Koffeinkonsum einschränken
* keine "komischen" Drinks vor dem Start


## Komplex Essen

* __keinen Traubenzucker__ weder im Training noch im Wettkampf
* __Zuckerkonsum einschränken__


## Aufstehen

* je früher desto besser
* wenn der Start 9:00 dann 5:00 aufstehen
* 2.5-3 Stunden vor dem Start frühstücken


## Frühstück am Marathontag

* keine Experimente zum Frühstück
* Frühstück mal am Tag eines langen Trainingslaufes ausprobieren
* gut Kauen
* nicht __mehr__ Essen (im Sinne von zu viel)


## Athletik vor dem Start

* leichtes lockern
* kein übertriebenes Dehnen
* Traben
* kein Animationsprogramm
* 5 min lockeres Einlaufen auch eine viertel Stunde vorher


## Das Rennen

* __nicht zu schnell loslaufen__
* von Anfang bis Ende __gleich schnell durchlaufen__
* __FOKUSIEREN__ auf den Lauf
* nicht reden
* die ideale Verpflegung beim Marathon ist das Frühstück und die Zielverpflegung
* Bananen, etc. zwischendurch vermeiden
* __Wasser!!!__
* Verdauung sollte (nicht) aktiv sein
* ISO-Getränke vermeiden
* Datteln
* GELs?


## Im Ziel

* Energie wieder auffüllen
* Ruhe -- Ruhe -- Ruhe
* lange Läufe weiter beibehalten
